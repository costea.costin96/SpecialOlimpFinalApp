package com.example.ccostea.specialoliapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ccostea.specialoliapp.R;
import com.example.ccostea.specialoliapp.people.Volunteer;

public class LoginActivity extends AppCompatActivity {
    // layout
EditText et_username;
EditText et_password;
Button btn_register;
Button btn_login;

//other
Intent intent;
public static Volunteer volunteer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initComponents();
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent= new Intent(getApplicationContext(),RegisterActivity.class);
                startActivity(intent);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateInput()){
                    volunteer=getVolunteerfromInput();
                    intent=new Intent(getApplicationContext(),MenuActivity.class);
                    startActivity(intent);
                }

            }
        });
    }
    private void initComponents(){
        et_password=findViewById(R.id.login_et_password);
        et_username=findViewById(R.id.login_et_username);
        btn_login=findViewById(R.id.login_btn_login);
        btn_register=findViewById(R.id.login_Register);

    }
    private boolean validateInput(){
        if(et_username.getText().toString().trim().isEmpty() && et_password.getText().toString().trim().isEmpty()){
            Toast.makeText(getApplicationContext(),"Please enter valid data", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
    private Volunteer getVolunteerfromInput(){
        Volunteer volunteer= new Volunteer();
        volunteer.setEmail(et_username.getText().toString());
        volunteer.setPassword(et_password.getText().toString());
        return volunteer;
    }
}
