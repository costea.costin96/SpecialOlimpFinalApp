package com.example.ccostea.specialoliapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.ccostea.specialoliapp.R;

public class MenuActivity extends AppCompatActivity {
    //layout
    Button btn_identify_participant;
    Button btn_register_participant;

    //other
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        initComponents();
        btn_register_participant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent= new Intent(getApplicationContext(),ParticipantRegisterActivity.class);
                startActivity(intent);
            }
        });

        btn_identify_participant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent= new Intent(getApplicationContext(),ParticipantIdentifierActivity.class);
                startActivity(intent);
            }
        });
    }
    private void initComponents(){
        btn_identify_participant=findViewById(R.id.menu_button_identify);
        btn_register_participant=findViewById(R.id.menu_button_register);

    }
}
