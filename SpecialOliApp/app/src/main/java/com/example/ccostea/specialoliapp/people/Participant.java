package com.example.ccostea.specialoliapp.people;

/**
 * Created by flanco on 01/04/2018.
 */

public class Participant {
    String name;
    String companionName;
    String idBracelet;
    String companionPhone;
    String sport;

    public Participant() {
    }

    public Participant(String name, String companionName, String idBracelet, String companionPhone, String sport) {
        this.name = name;
        this.companionName = companionName;
        this.idBracelet = idBracelet;
        this.companionPhone = companionPhone;
        this.sport = sport;
    }

    public Participant(String IdBracelet) {
        this.idBracelet = IdBracelet;
    }

    @Override
    public String toString() {
        return "Participant{" +
                "nume='" + name + '\'' +
                ", companionName='" + companionName + '\'' +
                ", idBracelet='" + idBracelet + '\'' +
                ", companionPhone='" + companionPhone + '\'' +
                ", sport='" + sport + '\'' +
                '}';
    }


}
