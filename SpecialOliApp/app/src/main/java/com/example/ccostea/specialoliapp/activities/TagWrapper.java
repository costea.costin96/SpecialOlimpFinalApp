package com.example.ccostea.specialoliapp.activities;

/**
 * Created by ccostea on 4/2/2018.
 */

class TagWrapper {
    private String id;
    TagTechList techList = new TagTechList();

    public TagWrapper(final String tagId) {
        id = tagId;
    }

    public final String getId() {
        return id;
    }
}