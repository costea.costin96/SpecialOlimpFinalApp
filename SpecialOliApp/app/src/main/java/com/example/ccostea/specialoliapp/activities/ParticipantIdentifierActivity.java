package com.example.ccostea.specialoliapp.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ccostea.specialoliapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class ParticipantIdentifierActivity extends AppCompatActivity {

        static private ArrayList<TagWrapper> tags = new ArrayList<TagWrapper>();
        static private int currentTagIndex = -1;

        private NfcAdapter adapter = null;
        private PendingIntent pendingIntent = null;

        private TextView currentTagView;
        private TextView currentTagId;


        @Override
        public void onCreate(final Bundle savedState) {
            super.onCreate(savedState);

            setContentView(R.layout.activity_identifier);

            currentTagView = (TextView) findViewById(R.id.currentTagView);
            currentTagId = (TextView) findViewById(R.id.identifier_tv_tagid);
            //currentTagView.setText("Loading...");
            adapter = NfcAdapter.getDefaultAdapter(this);

        }

        @Override
        public void onResume() {
            super.onResume();

            if (!adapter.isEnabled()) {
                Utils.showNfcSettingsDialog(this);
                return;
            }

            if (pendingIntent == null) {
                pendingIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

                currentTagView.setText("Scan a tag");
            }


            adapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }

        @Override
        public void onPause() {
            super.onPause();
            adapter.disableForegroundDispatch(this);
        }

        @Override
        public void onNewIntent(Intent intent) {
            Log.d("onNewIntent", "Discovered tag with intent " + intent);

            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String tagId = Utils.bytesToHex(tag.getId());
            currentTagId.setText(tagId);


        }




}
