package com.example.ccostea.specialoliapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ccostea.specialoliapp.R;

public class RegisterActivity extends AppCompatActivity {
    //layout
    EditText et_email;
    EditText et_password;
    EditText et_phone;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initComponents();
    }
    private void initComponents(){
        et_email=findViewById(R.id.register_edittext_name);
        et_password=findViewById(R.id.register_edittext_password);
        et_phone=findViewById(R.id.register_edittext_phone);

    }
    private boolean validateInput(){
        if(et_phone.getText().toString().trim().isEmpty() || et_password.getText().toString().trim().isEmpty() || et_phone.getText().toString().trim().isEmpty()){
            Toast.makeText(getApplicationContext(),"Please enter valid data",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
