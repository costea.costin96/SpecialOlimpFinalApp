package com.example.ccostea.specialoliapp.people;

/**
 * Created by flanco on 01/04/2018.
 */

public class Volunteer {
    private String email;
    private String password;
    private String phone;

    public Volunteer(String email, String password, String phone) {
        this.email = email;
        this.password = password;
        this.phone = phone;
    }

    public Volunteer() {
    }

    @Override
    public String toString() {
        return "Volunteer{" +
                "email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
